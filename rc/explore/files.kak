provide-module explore-files %{
  # Enable explore-files
  define-command explore-files-enable -docstring 'Enable explore-files' %{
    # Allow :edit with no argument to explore the buffer directory
    hook -group explore-files global RuntimeError "\d+:\d+: '(edit|e)': wrong argument count" %{
      evaluate-commands -save-regs 'd' %{
        # Save the buffer directory
        set-register d %sh(dirname "$kak_buffile")

        # Display message
        echo -markup "{Information}explore-files %reg{d}{Default}"

        # Start exploring the buffer directory
        explore-files %reg{d}
      }
    }

    # Allow editing directories
    hook -group explore-files global RuntimeError "\d+:\d+: '(?:edit|e)': (.+): is a directory" %{
      # Display message
      echo -markup "{Information}explore-files %val{hook_param_capture_1}{Default}"

      # Start exploring files
      explore-files %val{hook_param_capture_1}
    }

    # Make `gf` to work with directories
    hook -group explore-files global RuntimeError "unable to find file '(.+)'" %{
      # Display message
      echo -markup "{Information}explore-files %val{hook_param_capture_1}{Default}"

      # Start exploring files
      explore-files %val{hook_param_capture_1}
    }

    # Start Kakoune with a directory
    hook -group explore-files global KakBegin .* %{
      # Kakoune just started; wait for the client to be created.
      hook -once global ClientCreate .* %{
        try %{
          # Search the directories to edit in the *debug* buffer
          evaluate-commands -buffer '*debug*' %{
            # Select the directories
            execute-keys '%' 1s "^error while opening file '(.+?)':\n[^\n]+: is a directory$" <ret>

            # Display message in the client context
            evaluate-commands -client %val{hook_param} -- echo -markup "{Information}explore-files %reg{.}{Default}"

            # Start exploring files in the client context
            evaluate-commands -client %val{hook_param} -- explore-files %reg{.}
          }
        }
      }
    }
  }

  # Disable explore-files
  define-command explore-files-disable -docstring 'Disable explore-files' %{
    remove-hooks global explore-files
  }
}
